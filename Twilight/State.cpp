#include "State.h"

static twilight::engine_state_t _state;

twilight::engine_state_t* twilight::engine_state_t::instance() {
	return &_state;
}

void twilight::engine_state_t::init() {
	state = twilight::ENGINE_STATE_INIT;
	current = NULL;
	title = twilight::title_manager_t::instance();
	title->init();
	game = twilight::game_manager_t::instance();
	game->init();
	pause = twilight::pause_manager_t::instance();
	pause->init();
	credit = twilight::credit_manager_t::instance();
	credit->init();
	win = twilight::win_manager_t::instance();
	win->init();
	fail = twilight::fail_manager_t::instance();
	fail->init();
	transition(twilight::ENGINE_STATE_MAIN_MENU);
}

void twilight::engine_state_t::transition(uint32_t target) {
	if (current) {
		current->exit();
	}
	switch (target) {
	case twilight::ENGINE_STATE_MAIN_MENU:
		current = title;
		break;
	case twilight::ENGINE_STATE_GAME_RUN:
		current = game;
		break;
	case twilight::ENGINE_STATE_PAUSE:
		current = pause;
		break;
	case twilight::ENGINE_STATE_CREDIT:
		current = credit;
		break;
	case twilight::ENGINE_STATE_WIN:
		current = win;
		break;
	case twilight::ENGINE_STATE_FAIL:
		current = fail;
		break;
	}
	printf("Handing control to next state manager ...\n");
	if (current) {
		current->enter();
	}
}

void twilight::engine_state_t::update(double dt) {
	if (current) {
		current->update(dt);
	}
}

void twilight::engine_state_t::user_key(S2D_Event e) {
	if (current) {
		current->user_key(e);
	}
}

void twilight::engine_state_t::user_mouse(S2D_Event e) {
	if (current) {
		current->user_mouse(e);
	}
}

void twilight::engine_state_t::render() {
	if (current) {
		current->render();
	}
}
