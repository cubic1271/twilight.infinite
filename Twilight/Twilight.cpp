#define _CRT_SECURE_NO_WARNINGS

#include "State.h"
#include "Map.h"
#include <time.h>
#include <simple2d.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <cassert>
#include <Windows.h>

#define VERTEX_SHADER_PATH          "Resource/Shader/simple2d.vertex"
#define FRAGMENT_SHADER_PATH        "Resource/Shader/simple2d.color.fragment"
#define FRAGMENT_TEX_SHADER_PATH    "Resource/Shader/simple2d.texture.fragment"

using twilight::map_t;
using twilight::engine_state_t;

static double _frequency;
static double _last_frame;
static double _start_frame;
static LARGE_INTEGER _tmp;

static engine_state_t* state;
static int _do_shader_init = 1;

static GLuint shaderProgram;
static GLuint texShaderProgram;

static int _init_counter() {
	if (!QueryPerformanceFrequency(&_tmp)) {
		printf("Unable to determine performance counter frequency ... this is kind of bad.\n");
		return -1;
	}
	_frequency = double(_tmp.QuadPart);
	QueryPerformanceCounter(&_tmp);
	_last_frame = double(_tmp.QuadPart) / _frequency;
	_start_frame = _last_frame;
	return 0;
}

static char* _readfile(const char* path) {
	std::ifstream       file;
	std::stringstream   tbuf;
	file.open(path);
	tbuf << file.rdbuf();
	file.close();
	char* source = _strdup(tbuf.str().c_str());
	return source;
}

static int _shader_init() {
	char* vertexSource = _readfile(VERTEX_SHADER_PATH);
	char* fragmentSource = _readfile(FRAGMENT_SHADER_PATH);
	char* texFragmentSource = _readfile(FRAGMENT_TEX_SHADER_PATH);

	S2D_GL_UpdateShaderProgram(fragmentSource);
	S2D_GL_UpdateTexShaderProgram(texFragmentSource);
	S2D_GL_UpdateVertexProgram(vertexSource);

	free(vertexSource);
	free(fragmentSource);
	free(texFragmentSource);

	_do_shader_init = 0;
	return 0;
}

void render() {
	state->render();
}

void update() {
	shaderProgram = S2D_GL_GetShaderProgram();
	texShaderProgram = S2D_GL_GetTexShaderProgram();
	GLuint loc = glGetUniformLocation(texShaderProgram, "u_time_modifier");
	if (loc < 0) {
		printf("Fatal: unable to find 'elapsed time' uniform ... (program = %d)\n", texShaderProgram);
		exit(EXIT_FAILURE);
	}

	QueryPerformanceCounter(&_tmp);
	double total = (double(_tmp.QuadPart) - _start_frame) / (5 * _frequency);
	total = fabs(fmod(round(total * 1000), 1000) - 500);
	total = total / 500.0;
	glUniform1f(loc, (GLfloat)total);

	double curr = double(_tmp.QuadPart) / _frequency;
	double delta = curr - _last_frame;
	_last_frame = curr;

	state->update(delta);
}

static void _key_handler(S2D_Event e) {
	state->user_key(e);
}

static void _mouse_handler(S2D_Event e) {
	state->user_mouse(e);
}

int main(int argc, char* argv[]) {
	srand((unsigned int)time(0));
	if (_init_counter() < 0) {
		return -1;
	}

	printf("Loading resources ...\n");
	_shader_init();
	state = engine_state_t::instance();
	state->init();

	S2D_Window *window = S2D_CreateWindow(
		"twilight/infinite (MGJ 2018, TwilightEngine v0.01)", 1280, 720, update, render, 0
	);
	window->icon = "Resource/icon.png";
	window->on_key = _key_handler;
	window->on_mouse = _mouse_handler;

	assert(window);

	S2D_Show(window);
	return 0;
}
