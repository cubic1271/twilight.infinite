#include "State.h"

static twilight::fail_manager_t _fail;

void twilight::fail_manager_t::update(double dt) {

}

void twilight::fail_manager_t::render() {

}

void twilight::fail_manager_t::init() {

}

void twilight::fail_manager_t::enter() {

}

void twilight::fail_manager_t::exit() {

}

void twilight::fail_manager_t::user_key(S2D_Event e) {

}

void twilight::fail_manager_t::user_mouse(S2D_Event e) {

}

twilight::fail_manager_t* twilight::fail_manager_t::instance() {
	return &_fail;
}
