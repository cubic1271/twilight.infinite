#pragma once

#include <string>
#include <cstdint>
#include <simple2d.h>

namespace twilight {
	constexpr uint8_t TILE_TYPE_ENTRANCE = 0x00;
	constexpr uint8_t TILE_TYPE_EXIT = 0x01;
	constexpr uint8_t TILE_TYPE_OPEN = 0x02;
	constexpr uint8_t TILE_TYPE_WALL = 0x03;

	constexpr uint8_t TILE_DIRECTION_LEFT  = 0;
	constexpr uint8_t TILE_DIRECTION_RIGHT = 1;
	constexpr uint8_t TILE_DIRECTION_UP    = 2;
	constexpr uint8_t TILE_DIRECTION_DOWN  = 3;

	constexpr uint8_t TILE_TYPE_COUNT = 4;

	typedef struct tile_t {
		double       illumination;
		double       negativity;
		uint32_t     type;
		uint8_t      passable;
		uint8_t      unused[3];

		tile_t() : negativity(0.0), illumination(0.0), type(0), passable(0) 
		{ }
	} tile_t;

	typedef struct map_t {
		uint32_t           width;
		uint32_t           height;
		tile_t*            tiles;
		uint32_t           illumination_x;
		uint32_t           illumination_y;
		double             illumination_power;
		
		void           init(const uint32_t map_size_x, const uint32_t map_size_y);
		void           generate();
		tile_t*        tile_at(const uint32_t x, const uint32_t y);
		void           print();
		static map_t*  instance();
		void           update_illumination(const uint32_t source_x, const uint32_t source_y, const double power);
		int            can_move_to(const double target_x, const double target_y);
		int            is_exit(const double target_x, const double target_y);
		int            is_entrance(const double target_x, const double target_y);

	private:
		void           _illuminate(const uint32_t direction, const uint32_t curr_x, const uint32_t curr_y, const uint32_t turns);
	} map_t;
};
