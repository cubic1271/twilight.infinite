#include "State.h"
#include "Map.h"
#include <simple2d.h>
#include <cassert>

static twilight::game_manager_t _game;

#define MAP_X_OFFSET (45)
#define MAP_Y_OFFSET (30)

#define PLAYER_LIGHT_BASE  (2)
#define PLAYER_LIGHT_CAP   (0.3)
#define PLAYER_LIGHT_MOD   (0.1)



void twilight::game_manager_t::update(double dt) {
	twilight::map_t* map = twilight::map_t::instance();
	fps = 1.0 / dt;
	score += 51.32 * dt * (level / 2.5);

	map->update_illumination(player->x, player->y, player->light);

	if (move_left) {
		double new_x = player->x - dt * player->move_accel;
		if (map->can_move_to(new_x, player->y)) {
			player->x -= dt * player->move_accel;
		}
	}
	if (move_right) {
		double new_x = player->x + dt * player->move_accel;
		if (map->can_move_to(new_x, player->y)) {
			player->x += dt * player->move_accel;
		}
	}
	if (move_down) {
		double new_y = player->y + dt * player->move_accel;
		if (map->can_move_to(player->x, new_y)) {
			player->y += dt * player->move_accel;
		}
	}
	if (move_up) {
		double new_y = player->y - dt * player->move_accel;
		if (map->can_move_to(player->x, new_y)) {
			player->y -= dt * player->move_accel;
		}
	}

	if (map->is_entrance(player->x, player->y) && level == 1) {
		show_instructions = 1;
	}
	else {
		show_instructions = 0;
	}

	if (map->is_exit(player->x, player->y)) {
		next_level();
	}
}

void twilight::game_manager_t::render() {
	twilight::map_t* map = twilight::map_t::instance();

	int tile_width = floor_tile->width;
	int tile_height = floor_tile->height;

	S2D_Color tColor;

	for (int y = 0; y < map->height; ++y) {
		for (int x = 0; x < map->width; ++x) {
			tColor.a = 1.0;
			tColor.r = map->tile_at(x, y)->illumination;
			tColor.g = map->tile_at(x, y)->illumination;
			tColor.b = map->tile_at(x, y)->illumination;

			if (map->tile_at(x, y)->type == twilight::TILE_TYPE_WALL) {
				wall_tile->x = tile_width * x + MAP_X_OFFSET;
				wall_tile->y = tile_height * y + MAP_Y_OFFSET;
				wall_tile->color = tColor;
				S2D_DrawSprite(wall_tile);
			}
			else if (map->tile_at(x, y)->type == twilight::TILE_TYPE_OPEN) {
				floor_tile->x = tile_width * x + MAP_X_OFFSET;
				floor_tile->y = tile_height * y + MAP_Y_OFFSET;
				floor_tile->color = tColor;
				S2D_DrawSprite(floor_tile);
			}
			else if (map->tile_at(x, y)->type == twilight::TILE_TYPE_EXIT) {
				exit_tile->x = tile_width * x + MAP_X_OFFSET;
				exit_tile->y = tile_height * y + MAP_Y_OFFSET;
				exit_tile->color = tColor;
				S2D_DrawSprite(exit_tile);
			}
		}
	}

	player_sprite->x = player->x * tile_width + MAP_X_OFFSET;
	player_sprite->y = player->y * tile_height + MAP_Y_OFFSET;
	S2D_DrawSprite(player_sprite);

	S2D_SetText(fps_text, "FPS: %0.2f", fps);
	S2D_DrawText(fps_text);

	S2D_SetText(level_text, "Level %d", level);
	S2D_DrawText(level_text);

	S2D_SetText(score_text, "Score: %d", (int)score);
	S2D_DrawText(score_text);

	if (show_instructions) {
		S2D_SetText(instructions_text, "Escape the dream!  W,A,S,D to move ...");
		S2D_DrawText(instructions_text);
	}
}

void twilight::game_manager_t::next_level() {
	twilight::map_t* map = twilight::map_t::instance();
	map->generate();
	for (int x = 0; x < map->width; ++x) {
		for (int y = 0; y < map->height; ++y) {
			if (map->tile_at(x, y)->type == twilight::TILE_TYPE_ENTRANCE) {
				player->x = (double)x + 0.2;
				player->y = (double)y + 0.2;
			}
		}
	}
	level++;
	player->light = PLAYER_LIGHT_BASE - PLAYER_LIGHT_MOD * level;
	if (player->light < PLAYER_LIGHT_CAP) {
		player->light = PLAYER_LIGHT_CAP;
	}
}

void twilight::game_manager_t::reset() {
	level = 0;
	score = 0;
	next_level();
}

void twilight::game_manager_t::init() {
	move_up = 0;
	move_left = 0;
	move_right = 0;
	move_down = 0;

	twilight::map_t* map = twilight::map_t::instance();
	player = new twilight::player_t;
	map->init(36, 18);
	player->move_accel = 2.0;
	reset();

	fps_text = S2D_CreateText("Resource/Game/Adler.ttf", "", 12);
	level_text = S2D_CreateText("Resource/Game/Adler.ttf", "", 14);
	score_text = S2D_CreateText("Resource/Game/Adler.ttf", "", 12);
	instructions_text = S2D_CreateText("Resource/Game/Adler.ttf", "", 16);
	instructions_text->color.r = 1.0;
	instructions_text->color.g = 0.0;
	instructions_text->color.b = 0.0;

	floor_tile = S2D_CreateSprite("Resource/Game/floor_32.png");
	wall_tile = S2D_CreateSprite("Resource/Game/wall_32.png");
	exit_tile = S2D_CreateSprite("Resource/Game/exit_32.png");
	player_sprite = S2D_CreateSprite("Resource/Game/Player.png");

	music = S2D_CreateMusic("Resource/Game/Shadowlands_4.mp3");

	assert(music != NULL);

	assert(fps_text != NULL);
	assert(level_text != NULL);
	assert(score_text != NULL);
	assert(player_sprite != NULL);

	assert(floor_tile != NULL);
	assert(wall_tile != NULL);
	assert(exit_tile != NULL);

	fps_text->x = 10;
	fps_text->y = 10;

	level_text->x = 1160;
	level_text->y = 690;

	score_text->x = 1160;
	score_text->y = 10;

	instructions_text->x = 10;
	instructions_text->y = 690;
}

void twilight::game_manager_t::enter() {
	printf("Game state manager is starting up ...\n");
	S2D_PlayMusic(music, true);
}

void twilight::game_manager_t::exit() {
	S2D_StopMusic();
}

void twilight::game_manager_t::user_key(S2D_Event e) {

	std::string key = std::string(e.key);
	if (e.type == S2D_KEY_DOWN) {
		if (key == "W") {
			move_up = 1;
		}
		if (key == "A") {
			move_left = 1;
		}
		if (key == "S") {
			move_down = 1;
		}
		if (key == "D") {
			move_right = 1;
		}
	}
	else if (e.type == S2D_KEY_UP) {
		if (key == "W") {
			move_up = 0;
		}
		if (key == "A") {
			move_left = 0;
		}
		if (key == "S") {
			move_down = 0;
		}
		if (key == "D") {
			move_right = 0;
		}
	}
}

void twilight::game_manager_t::user_mouse(S2D_Event e) {

}

twilight::game_manager_t* twilight::game_manager_t::instance() {
	return &_game;
}
