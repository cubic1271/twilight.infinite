#pragma once

#include <simple2d.h>
#include <cstdint>
#include "Particle.h"
#include "Util.h"
#include "GameObject.h"

#define _START_X 1000.0
#define _START_Y 350.0
#define _START_WIDTH 230.0
#define _START_HEIGHT 50.0

#define _CREDITS_X       1000.0
#define _CREDITS_Y       465.0
#define _CREDITS_WIDTH   240.0
#define _CREDITS_HEIGHT  50.0

#define _EXIT_X         1020.0
#define _EXIT_Y          590.0
#define _EXIT_WIDTH      185.0
#define _EXIT_HEIGHT      50.0

namespace twilight {
	constexpr uint32_t ENGINE_STATE_INIT       = 0;
	constexpr uint32_t ENGINE_STATE_MAIN_MENU  = 1;
	constexpr uint32_t ENGINE_STATE_GAME_RUN   = 2;
	constexpr uint32_t ENGINE_STATE_PAUSE      = 3;
	constexpr uint32_t ENGINE_STATE_FAIL       = 4;
	constexpr uint32_t ENGINE_STATE_WIN        = 5;
	constexpr uint32_t ENGINE_STATE_CREDIT     = 6;

	typedef struct state_entry_t {
		virtual uint32_t state() = 0;
		virtual void update(double dt) = 0;
		virtual void render() = 0;
		virtual void init() = 0;
		virtual void enter() = 0;
		virtual void exit() = 0;
		virtual void user_key(S2D_Event e) = 0;
		virtual void user_mouse(S2D_Event e) = 0;
	} state_entry_t;

	typedef struct title_manager_t : public state_entry_t {
		S2D_Image* background;
		S2D_Music* music;
		S2D_Sound* start_sound;

		rect_t     start_button;
		rect_t     credits_button;
		rect_t     exit_button;

		uint32_t state() {
			return ENGINE_STATE_MAIN_MENU;
		}
		void init();
		void update(double dt);
		void render();
		void enter();
		void exit();
		void user_key(S2D_Event e);
		void user_mouse(S2D_Event e);

		static title_manager_t* instance();
	} title_manager_t;

	typedef struct game_manager_t : public state_entry_t {
		int          move_left;
		int          move_right;
		int          move_up;
		int          move_down;
		int          show_instructions;

		double       fps;
		int          level;
		double       score;
		S2D_Text*    fps_text;
		S2D_Text*    level_text;
		S2D_Text*    score_text;
		S2D_Text*    instructions_text;
		S2D_Sprite*  floor_tile;
		S2D_Sprite*  wall_tile;
		S2D_Sprite*  exit_tile;
		S2D_Sprite*  entrance_tile;
		S2D_Sprite*  player_sprite;
		S2D_Music*   music;

		player_t*    player;

		uint32_t state() {
			return ENGINE_STATE_GAME_RUN;
		}
		void init();
		void update(double dt);
		void render();
		void enter();
		void exit();
		void user_key(S2D_Event e);
		void user_mouse(S2D_Event e);

		void next_level();
		void reset();

		static game_manager_t* instance();
	} game_manager_t;

	typedef struct pause_manager_t : public state_entry_t {

		uint32_t state() {
			return ENGINE_STATE_PAUSE;
		}
		void init();
		void update(double dt);
		void render();
		void enter();
		void exit();
		void user_key(S2D_Event e);
		void user_mouse(S2D_Event e);

		static pause_manager_t* instance();
	} pause_manager_t;

	typedef struct fail_manager_t : public state_entry_t {

		uint32_t state() {
			return ENGINE_STATE_FAIL;
		}
		void init();
		void update(double dt);
		void render();
		void enter();
		void exit();
		void user_key(S2D_Event e);
		void user_mouse(S2D_Event e);

		static fail_manager_t* instance();
	} fail_manager_t;

	typedef struct win_manager_t : public state_entry_t {

		uint32_t state() {
			return ENGINE_STATE_WIN;
		}
		void init();
		void update(double dt);
		void render();
		void enter();
		void exit();
		void user_key(S2D_Event e);
		void user_mouse(S2D_Event e);

		static win_manager_t* instance();
	} win_manager_t;

	typedef struct credit_manager_t : public state_entry_t {
		S2D_Image* background;
		S2D_Music* music;

		uint32_t state() {
			return ENGINE_STATE_CREDIT;
		}

		void init();
		void update(double dt);
		void render();
		void enter();
		void exit();
		void user_key(S2D_Event e);
		void user_mouse(S2D_Event e);

		static credit_manager_t* instance();
	} credit_manager_t;

	typedef struct engine_state_t {
		uint32_t state;
		uint32_t target;
		title_manager_t* title;
		game_manager_t* game;
		pause_manager_t* pause;
		fail_manager_t* fail;
		win_manager_t* win;
		credit_manager_t* credit;

		state_entry_t* current;

		void init();
		void transition(uint32_t target);
		void user_key(S2D_Event e);
		void user_mouse(S2D_Event e);
		void update(double dt);
		void render();
		static engine_state_t* instance();
	} engine_state_t;
};
