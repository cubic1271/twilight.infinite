#version 330

// http://alex-charlton.com/posts/Dithering_on_the_GPU/ - derivative of the code there

const float indexMatrix4x4[16] = float[](0.0,  8.0,  2.0,  10.0,
                                     12.0, 4.0,  14.0, 6.0,
                                     3.0,  11.0, 1.0,  9.0,
                                     15.0, 7.0,  13.0, 5.0);

float indexValue() {
    int x = int(mod(gl_FragCoord.x, 4.0));
    int y = int(mod(gl_FragCoord.y, 4.0));
    return indexMatrix4x4[(x + y * 4)] / 16.0;
}

float dither(float color) {
    float closestColor = (color < 0.5) ? 0.0 : 1.0;
    float secondClosestColor = 1.0 - closestColor;
    float d = indexValue();
    float distance = abs(closestColor - color);
    return (distance < d) ? closestColor : secondClosestColor;
}

void mainImage( out vec4 fragColor )
{
    if(col[0] > 0.5) {
    	fragColor = vec4(vec3(0.5, 0, 0), 1.0);
    }
    else {
	    // Convert to monochrome
    	float mono = 0.2125 * col[0] + 0.7154 * col[1] + 0.0721 * col[2];
    	mono = dither(mono);
        // Output to screen
        fragColor = vec4(vec3(mono, mono, mono), 1.0);
    }
    
}
