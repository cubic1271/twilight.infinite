#pragma once

#include "Map.h"
#include <vector>
#include <string>
#include <simple2d.h>

namespace twilight {
	typedef struct manager_t {
		std::string tile_path[TILE_TYPE_COUNT];
		std::string player_path;

		S2D_Sprite tile[TILE_TYPE_COUNT];
		S2D_Sprite player;

		void init();
	} manager_t;
};
