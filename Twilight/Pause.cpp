#include "State.h"

static twilight::pause_manager_t _pause;

void twilight::pause_manager_t::update(double dt) {

}

void twilight::pause_manager_t::render() {

}

void twilight::pause_manager_t::init() {

}

void twilight::pause_manager_t::enter() {

}

void twilight::pause_manager_t::exit() {

} 

void twilight::pause_manager_t::user_key(S2D_Event e) {

}

void twilight::pause_manager_t::user_mouse(S2D_Event e) {

}

twilight::pause_manager_t* twilight::pause_manager_t::instance() {
	return &_pause;
}
