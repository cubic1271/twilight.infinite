#pragma once

#include <vector>
#include "Util.h"

namespace twilight {

	struct particle_system_t;
	struct particle_t;

	typedef struct particle_manager_t {
		typedef std::vector<particle_system_t*> particle_system_container_t;

		void update(double delta);
	} particle_manager_t;

	typedef struct particle_system_t {
		typedef std::vector<particle_t*> particle_container_t;

		vector2_t  location;

		vector2_t  emit_velocity_min;
		vector2_t  emit_velocity_max;
		vector2_t  emit_acceleration_min;
		vector2_t  emit_acceleration_max;

		double     rate;
		double     life;
		double     decay;

		void update(double delta);
	} particle_system_t;

	typedef struct particle_t {
		vector2_t  location;
		vector2_t  velocity;
		vector2_t  acceleration;

		double     life;

		void update(double delta);
	} particle_t;

}
