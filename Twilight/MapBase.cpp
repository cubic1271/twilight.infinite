#include "Map.h"

#include <cstdlib>
#include <cstdio>
#include <cassert>

#define LIGHT_NUM_TURNS            (2)
#define MIN_MAP_DISTANCE_FACTOR  (0.4)

// https://raw.githubusercontent.com/joewing/maze/master/maze.c

static void _build_call(twilight::map_t* map, const int x, const int y);

static void _build_map(twilight::map_t* map) {
	int x = 0;
	int y = 0;

	do {
		x = rand() % map->width;
	} while (x % 2 == 0);

	do {
		y = rand() % map->height;
	} while (y % 2 == 0);

	map->tile_at(x, y)->type = twilight::TILE_TYPE_OPEN;

	_build_call(map, x, y);
}

static void _build_call(twilight::map_t* map, const int x, const int y) {
	int width = map->width;
	int height = map->height;
	int direction[4] = { 0, 1, 2, 3 };

	for (int i = 0; i < 10; ++i) {
		int src = rand() % 4;
		int dst = rand() % 4;
		int tmp = direction[dst];
		direction[dst] = direction[src];
		direction[src] = tmp;
	}

	for (int i = 0; i < 4; ++i) {
		switch (direction[i]) {
		case 0:
			if (y - 2 <= 0) {
				continue;
			}
			if (map->tile_at(x, y - 2)->type == twilight::TILE_TYPE_WALL) {
				map->tile_at(x, y - 2)->type = twilight::TILE_TYPE_OPEN;
				map->tile_at(x, y - 1)->type = twilight::TILE_TYPE_OPEN;
				_build_call(map, x, y - 2);
			}
			break;
		case 1:
			if (y + 2 >= height - 1) {
				continue;
			}
			if (map->tile_at(x, y + 2)->type == twilight::TILE_TYPE_WALL) {
				map->tile_at(x, y + 2)->type = twilight::TILE_TYPE_OPEN;
				map->tile_at(x, y + 1)->type = twilight::TILE_TYPE_OPEN;
				_build_call(map, x, y + 2);
			}
			break;
		case 2:
			if (x + 2 >= width - 1) {
				continue;
			}
			if (map->tile_at(x + 2, y)->type == twilight::TILE_TYPE_WALL) {
				map->tile_at(x + 2, y)->type = twilight::TILE_TYPE_OPEN;
				map->tile_at(x + 1, y)->type = twilight::TILE_TYPE_OPEN;
				_build_call(map, x + 2, y);
			}
			break;
		case 3:
			if (x - 2 <= 0) {
				continue;
			}
			if (map->tile_at(x - 2, y)->type == twilight::TILE_TYPE_WALL) {
				map->tile_at(x - 2, y)->type = twilight::TILE_TYPE_OPEN;
				map->tile_at(x - 1, y)->type = twilight::TILE_TYPE_OPEN;
				_build_call(map, x - 2, y);
			}
			break;
		default:
			printf("Generation failed - unknown direction value of %d\n", direction[i]);
			assert(false);
		}
	}
}

void twilight::map_t::init(const uint32_t map_size_x, const uint32_t map_size_y) {

	width = map_size_x;
	if (map_size_x % 2 == 0) {
		++width;
	}

	height = map_size_y;
	if (map_size_y % 2 == 0) {
		++height;
	}

	tiles = new tile_t[width * height];
}

void twilight::map_t::generate() {
	uint32_t i;

	for (i = 0; i < width * height; i++) {
		tiles[i].type = twilight::TILE_TYPE_WALL;
	}
	
	printf("Generating new map (width=%d, height=%d) ...\n", width, height);
	_build_map(this);

	double dist = 0.0;
	double ent_x = 0.0;
	double ent_y = 0.0;
	double exit_x = 0.0;
	double exit_y = 0.0;

	printf("Done.  Generating entrance and exit ...\n");
	while (dist < (sqrt(width * width + height * height)) * MIN_MAP_DISTANCE_FACTOR) {
		ent_x = rand() % width;
		ent_y = rand() % height;
		exit_x = rand() % width;
		exit_y = rand() % height;

		if (tile_at((uint32_t)ent_x, (uint32_t)ent_y)->type != TILE_TYPE_OPEN ||
			tile_at((uint32_t)exit_x, (uint32_t)exit_y)->type != TILE_TYPE_OPEN) {
			continue;
		}

		double dx = exit_x - ent_x;
		double dy = exit_y - ent_y;

		dist = sqrt(dx * dx + dy * dy);
	}

	printf("Entrance is: %0.0f, %0.0f\n", ent_x, ent_y);
	printf("Exit is: %0.0f, %0.0f\n", exit_x, exit_y);

	tile_at((uint32_t)ent_x, (uint32_t)ent_y)->type = TILE_TYPE_ENTRANCE;
	tile_at((uint32_t)exit_x, (uint32_t)exit_y)->type = TILE_TYPE_EXIT;
}

twilight::tile_t* twilight::map_t::tile_at(const uint32_t x, const uint32_t y) {
	if (x < 0 || y < 0 || x >= width || y >= height) {
		return NULL;
	}

	return &tiles[y * width + x];
}

int twilight::map_t::can_move_to(const double x, const double y) {
	if (x < 0 || x >= width) {
		return 0;
	}
	if (y < 0 || y >= height) {
		return 0;
	}
	if (tile_at(round(x), round(y))->type == TILE_TYPE_WALL
		|| tile_at(x, y)->type == TILE_TYPE_WALL) {
		return 0;
	}

	return 1;
}

int twilight::map_t::is_exit(const double x, const double y) {
	if (x < 0 || x >= width) {
		return 0;
	}
	if (y < 0 || y >= height) {
		return 0;
	}
	if (tile_at(round(x), round(y))->type == TILE_TYPE_EXIT
		|| tile_at(x, y)->type == TILE_TYPE_EXIT) {
		return 1;
	}

	return 0;
}

int twilight::map_t::is_entrance(const double x, const double y) {
	if (x < 0 || x >= width) {
		return 0;
	}
	if (y < 0 || y >= height) {
		return 0;
	}

	if (tile_at(round(x), round(y))->type == TILE_TYPE_ENTRANCE
		|| tile_at(x, y)->type == TILE_TYPE_EXIT) {
		return 1;
	}

	return 0;
}

void twilight::map_t::print() {
	uint32_t x = 0;
	uint32_t y = 0;

	for (y = 0; y < height; ++y) {
		for (x = 0; x < width; ++x) {
			switch (tiles[y * width + x].type) {
			case twilight::TILE_TYPE_ENTRANCE:
				printf("%%");
				break;
			case twilight::TILE_TYPE_EXIT:
				printf("^");
				break;
			case twilight::TILE_TYPE_WALL:
				printf("*");
				break;
			case twilight::TILE_TYPE_OPEN:
				printf("+");
				break;
			default:
				printf("?");
				break;
			}
		}
		printf("\n");
	}
}

void twilight::map_t::_illuminate(const uint32_t direction, const uint32_t curr_x, const uint32_t curr_y, const uint32_t turns) {
	const uint32_t source_x = illumination_x;
	const uint32_t source_y = illumination_y;
	const double power = illumination_power;
	if (turns >= LIGHT_NUM_TURNS) {
		return;
	}

	if (curr_x >= width || curr_x < 0) {
		return;
	}
	if (curr_y >= height || curr_y < 0) {
		return;
	}

	double light_dist = sqrt((source_x - curr_x) * (source_x - curr_x) + (source_y - curr_y) * (source_y - curr_y));
	double illum = power / light_dist;
	if (turns > 0) {
		illum /= 4 * turns;
	}
	if (illum > tile_at(curr_x, curr_y)->illumination) {
		tile_at(curr_x, curr_y)->illumination = illum;
	}

	if (tile_at(curr_x, curr_y)->illumination > 0.8) {
		tile_at(curr_x, curr_y)->illumination = 0.8;
	}
	if (tile_at(curr_x, curr_y)->illumination <= 0) {
		tile_at(curr_x, curr_y)->illumination = 0.0;
	}

	// Keep going 'til we hit a wall
	if (tile_at(curr_x, curr_y)->type == TILE_TYPE_WALL) {
		return;
	}
	switch (direction) {
	case TILE_DIRECTION_LEFT:
		_illuminate(direction, curr_x - 1, curr_y, turns);
		_illuminate(TILE_DIRECTION_UP, curr_x, curr_y - 1, turns + 1);
		_illuminate(TILE_DIRECTION_DOWN, curr_x, curr_y + 1, turns + 1);
		break;
	case TILE_DIRECTION_RIGHT:
		_illuminate(direction, curr_x + 1, curr_y, turns);
		_illuminate(TILE_DIRECTION_UP, curr_x, curr_y - 1, turns + 1);
		_illuminate(TILE_DIRECTION_DOWN, curr_x, curr_y + 1, turns + 1);
		break;
	case TILE_DIRECTION_DOWN:
		_illuminate(direction, curr_x, curr_y - 1, turns);
		_illuminate(TILE_DIRECTION_LEFT, curr_x - 1, curr_y, turns + 1);
		_illuminate(TILE_DIRECTION_RIGHT, curr_x + 1, curr_y, turns + 1);
		break;
	case TILE_DIRECTION_UP:
		_illuminate(direction, curr_x, curr_y + 1, turns);
		_illuminate(TILE_DIRECTION_LEFT, curr_x - 1, curr_y, turns + 1);
		_illuminate(TILE_DIRECTION_RIGHT, curr_x + 1, curr_y, turns + 1);
		break;
	}
}

void twilight::map_t::update_illumination(const uint32_t source_x, const uint32_t source_y, const double power) {
	illumination_x = source_x;
	illumination_y = source_y;
	illumination_power = power;

	// Clear previous illumination values ...
	for (int x = 0; x < width; ++x) {
		for (int y = 0; y < height; ++y) {
			tile_at(x, y)->illumination = 0;
		}
	}

	_illuminate(TILE_DIRECTION_LEFT, source_x - 1, source_y, 0);
	_illuminate(TILE_DIRECTION_RIGHT, source_x + 1, source_y, 0);
	_illuminate(TILE_DIRECTION_DOWN, source_x, source_y - 1, 0);
	_illuminate(TILE_DIRECTION_UP, source_x, source_y + 1, 0);
}

static twilight::map_t _map;

twilight::map_t* twilight::map_t::instance() {
	return &_map;
}
