#pragma once

#include <stdint.h>

namespace twilight {
	typedef struct vector2_t {
		double x;
		double y;

		vector2_t() : x(0), y(0) { }
		vector2_t(double x, double y) : x(x), y(y) { }
		vector2_t(const vector2_t& vec) : x(vec.x), y(vec.y) { }
	} vector2_t;

	typedef struct rect_t {
		double x;
		double y;
		double width;
		double height;

		rect_t() : x(0), y(0), width(0), height(0) { }

		rect_t(double x, double y, double width, double height) : x(x), y(y), width(width), height(height) { }

		uint8_t contains(double x, double y) {
			return (x > this->x && x < (this->x + width))
				&& (y > this->y && y < (this->y + height));
		}

	} rect_t;

}
