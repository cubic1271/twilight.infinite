#pragma once

namespace twilight {
	typedef struct player_t {
		double health;
		double x;
		double y;
		double move_accel;
		double light;
	} player_t;

	typedef struct enemy_t {
		double health;
		double x;
		double y;
		double move_accel;
		double light;
	} enemy_t;
}
