#include <simple2d.h>
#include "State.h"
#include <cassert>

using twilight::vector2_t;

static twilight::title_manager_t _title;

void twilight::title_manager_t::update(double dt) {

}

void twilight::title_manager_t::render() {
	S2D_DrawImage(background);
}

void twilight::title_manager_t::init() {
	background = S2D_CreateImage("Resource/Title/Title.png");
	music = S2D_CreateMusic("Resource/Title/Giant_Wyrm.mp3");
	start_sound = S2D_CreateSound("Resource/Title/bell.wav");

	if (NULL == music) {
		printf("%s", SDL_GetError());
	}

	start_button = rect_t(_START_X, _START_Y, _START_WIDTH, _START_HEIGHT);
	credits_button = rect_t(_CREDITS_X, _CREDITS_Y, _CREDITS_WIDTH, _CREDITS_HEIGHT);
	exit_button = rect_t(_EXIT_X, _EXIT_Y, _EXIT_WIDTH, _EXIT_HEIGHT);

	assert(background != NULL);
	assert(music != NULL);
	assert(start_sound != NULL);
}

void twilight::title_manager_t::enter() {
	printf("Handing control over to the title manager ...\n");
	S2D_PlayMusic(music, true);
}

void twilight::title_manager_t::exit() {
	printf("Title manager is shutting down ...\n");
	S2D_StopMusic();
}

void twilight::title_manager_t::user_key(S2D_Event e) {
	switch (e.type) {
	case S2D_KEY_DOWN:
		printf("User pressed: %s\n", e.key);
		if (std::string(e.key) == "Escape") {
			std::exit(EXIT_SUCCESS);
		}
		break;
	}
}

void twilight::title_manager_t::user_mouse(S2D_Event e) {
	switch (e.type) {
	case S2D_MOUSE_DOWN:
		printf("User clicked at: %d, %d\n", e.x, e.y);
		if (start_button.contains(e.x, e.y)) {
			printf("Start button selected - off we go.\n");
			engine_state_t* state = engine_state_t::instance();
			S2D_PlaySound(start_sound);
			state->transition(ENGINE_STATE_GAME_RUN);
		}
		if (credits_button.contains(e.x, e.y)) {
			printf("Credits button selected - off we go.\n");
			engine_state_t* state = engine_state_t::instance();
			state->transition(ENGINE_STATE_CREDIT);
		}
		if (exit_button.contains(e.x, e.y)) {
			printf("Exit button selected - goodbye.\n");
			std::exit(EXIT_SUCCESS);
		}
		break;
	}
}

twilight::title_manager_t* twilight::title_manager_t::instance() {
	return &_title;
}
