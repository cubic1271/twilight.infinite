#include "State.h"

static twilight::win_manager_t _win;

void twilight::win_manager_t::update(double dt) {

}

void twilight::win_manager_t::render() {

}

void twilight::win_manager_t::init() {

}

void twilight::win_manager_t::enter() {

}

void twilight::win_manager_t::exit() {

}

void twilight::win_manager_t::user_key(S2D_Event e) {

}

void twilight::win_manager_t::user_mouse(S2D_Event e) {

}

twilight::win_manager_t* twilight::win_manager_t::instance() {
	return &_win;
}
