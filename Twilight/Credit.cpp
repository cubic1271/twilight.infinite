#include "State.h"
#include <cassert>

static twilight::credit_manager_t _credit;

void twilight::credit_manager_t::update(double dt) {

}

void twilight::credit_manager_t::render() {
	S2D_DrawImage(background);
}

void twilight::credit_manager_t::init() {
	background = S2D_CreateImage("Resource/Credits/Credits.png");
	music = S2D_CreateMusic("Resource/Credits/Gregorian_Chant.mp3");
	assert(background != NULL);
	assert(music != NULL);
}

void twilight::credit_manager_t::enter() {
	printf("Credits state manager is starting up ...\n");
	S2D_PlayMusic(music, true);
}

void twilight::credit_manager_t::exit() {
	printf("Credits state manager is shutting down ...\n");
	S2D_StopMusic();
}

void twilight::credit_manager_t::user_key(S2D_Event e) {
	switch (e.type) {
	case S2D_KEY_DOWN:
		if (std::string(e.key) == "Escape") {
			printf("Escape key pressed - returning to title screen.\n");
			engine_state_t* state = engine_state_t::instance();
			state->transition(ENGINE_STATE_MAIN_MENU);
		}
	}
}

void twilight::credit_manager_t::user_mouse(S2D_Event e) {
	
}

twilight::credit_manager_t* twilight::credit_manager_t::instance() {
	return &_credit;
}
